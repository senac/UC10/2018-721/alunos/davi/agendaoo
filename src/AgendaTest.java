
import java.util.List;

public class AgendaTest {

    public static void main(String[] args) {

        Pessoa p = new Pessoa("Jose da Couves", "(27) 9999-9999");
        Pessoa p2 = new Pessoa("Silas", "(27) 9999-8888");
        Pessoa p3 = new Pessoa("Raphael", "(27) 9999-8888");
        Pessoa p4 = new Pessoa("Jonas", "(27) 9999-8888");
        Pessoa p5 = new Pessoa("Yuri", "(27) 9999-8888");
        Pessoa p6 = new Pessoa("Jhony", "(27) 9999-8888");
        Pessoa p7 = new Pessoa("Marcelo", "(27) 9999-8888");
        Pessoa p8 = new Pessoa("Samuel", "(27) 9999-8888");
        Pessoa p9 = new Pessoa("Dick", "(27) 9999-8888");
          
        Agenda agenda = new Agenda() ; 
        agenda.adicionarContato(p) ; 
        agenda.adicionarContato(p2) ; 
        agenda.adicionarContato(p3) ; 
        agenda.adicionarContato(p4) ; 
        agenda.adicionarContato(p5) ; 
        agenda.adicionarContato(p6) ; 
        agenda.adicionarContato(p7) ; 
        agenda.adicionarContato(p8) ; 
        agenda.adicionarContato(p9) ; 
        
        System.out.println("Contatos:" + agenda.getQuantidadeContatos());
        agenda.removerContato(p3);
        agenda.removerContato(p5);
        agenda.removerContato(p9);
        System.out.println("Contatos:" + agenda.getQuantidadeContatos());
        agenda.removerContato("Jose da Couves");
        System.out.println("Contatos:" + agenda.getQuantidadeContatos());
        
        System.out.println("Jose esta na agenda ? "  + agenda.isContatoNaAgenda(p));
        System.out.println("Silas esta na agenda ? "  + agenda.isContatoNaAgenda(p2));

    }

}
