
import java.util.ArrayList;
import java.util.List;

public class Agenda {

    private String nome;
    private String cor;
    private List<Pessoa> contatos;

    public Agenda() {
        this.contatos = new ArrayList<>();
    }

    public Agenda(String nome) {
        this();
        this.nome = nome;
    }

    public Agenda(String nome, String cor) {
        this(nome);
        this.cor = cor;

    }

    public void adicionarContato(Pessoa p) {

        if (!this.isContatoNaAgenda(p)) {
            this.contatos.add(p);
        } else {
            throw new RuntimeException("Contato ja existente !");
        }

    }

    public void removerContato(Pessoa p) {
        if (this.isContatoNaAgenda(p)) {
            this.contatos.remove(p);
        } else {
            throw new RuntimeException("Contato não esta na agenda !");
        }
    }

    public void removerContato(String nome) {

        for(int i = 0 ; i < this.contatos.size() ; i++ ){
            if(this.contatos.get(i).getNome().equalsIgnoreCase(nome)){
                this.contatos.remove(i);
            }
        }
    }
    
    public int getQuantidadeContatos(){
        return  this.contatos.size() ; 
    }
    
    public Pessoa buscarContato(String nome){
        for(int i = 0 ; i < this.getQuantidadeContatos() ; i ++){
            if(this.contatos.get(i).getNome().equals(nome)){
                return this.contatos.get(i) ; 
            }
        }
        
        return null ; 
    }
    
    public boolean isContatoNaAgenda(Pessoa p ){
        return  this.contatos.contains(p) ; 
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

}
