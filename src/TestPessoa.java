
import java.util.ArrayList;
import java.util.List;

public class TestPessoa {

    public static void main(String[] args) {

        Pessoa p = new Pessoa("Jose da Couves", "(27) 9999-9999");
        Pessoa p2 = new Pessoa("Silas", "(27) 9999-8888");
        Pessoa p3 = new Pessoa("Raphael", "(27) 9999-8888");
        Pessoa p4 = new Pessoa("Jonas", "(27) 9999-8888");
        Pessoa p5 = new Pessoa("Yuri", "(27) 9999-8888");
        Pessoa p6 = new Pessoa("Jhony", "(27) 9999-8888");
        Pessoa p7 = new Pessoa("Marcelo", "(27) 9999-8888");
        Pessoa p8 = new Pessoa("Samuel", "(27) 9999-8888");
        Pessoa p9 = new Pessoa("Dick", "(27) 9999-8888");
        Pessoa p0 = new Pessoa("Bruno", "(27) 9999-8888");
        Pessoa p11 = new Pessoa("Alex", "(27) 9999-8888");

        List<Pessoa> listaDePessoas = new ArrayList<>();
        listaDePessoas.add(p);
        listaDePessoas.add(p2);
        listaDePessoas.add(p3);
        listaDePessoas.add(p4);
        listaDePessoas.add(p5);
        listaDePessoas.add(p6);
        listaDePessoas.add(p7);
        listaDePessoas.add(p8);
        listaDePessoas.add(p9);
        listaDePessoas.add(p0);
        listaDePessoas.add(p11);

        System.out.println(listaDePessoas.size());
        System.out.println(p2);

        for (Pessoa x : listaDePessoas) {
            System.out.println(x);
        }

        Pessoa pX = new Pessoa("Jose da Couves", "(27) 9999-9999");
        Pessoa pY = new Pessoa("silas", "(27) 88888-8888");

        System.out.println("Jose esta na lista ?");
        System.out.println(listaDePessoas.contains(pX));

        System.out.println("Silas esta na lista ?");
        System.out.println(listaDePessoas.contains(pY));

        
        listaDePessoas.remove(pX);
        
        System.out.println("Jose esta na lista ?");
        System.out.println(listaDePessoas.contains(pX));


        /*
        for (int i = 0; i < listaDePessoas.size(); i++) {
            System.out.println(listaDePessoas.get(i));
        }*/
    }

}
